export class InitEnums001 {
  public async up(client): Promise<void> {
    await client.query(
      `CREATE TABLE "status" (
        "id" int(11) NOT NULL AUTO_INCREMENT,
        "status_name" varchar(45) NOT NULL,
        "description" varchar(500) DEFAULT NULL,
        PRIMARY KEY("id")
      ) AUTO_INCREMENT=5`
    );

    await client.query(
      ``
    );
  }

  public async down(client): Promise<void> {
    await client.query(
      ``
    );
  }
}