INSERT INTO "status"
VALUES
(1, 'N/A', 'Not available or applicable.'),
(2, 'TBA', 'To be added.'),
(3, 'IP', 'In progress.'),
(4, 'Complete', 'Complete.');