INSERT INTO "camp_type"
VALUES
(1,'Roadside','Sleeping overnight, but cannot leave shelter up, or may be limited to no tents.'),
(2,'Bivy','No tents, or marginal for tents.'),
(3,'Primitive','Tents, but undeveloped.'),
(4,'Campground','Tents, developed.'),
(5,'RV Park','Basically a parking lot, fee needed.'),
(6,'Hut','Enclosed shelter or hut.'),
(7,'Primitive - Beach','Tents, but undeveloped, on a beach.'),
(8,'Chickee','Wooden platform suspended above the water.'),
(9,'LeanTo','Roof and walls structure for tents.');
