INSERT INTO "objective_hazard"
VALUES
(1,'Avalanche','Prone to avalanching.'),
(2,'Rockfall','Incidence of rockfall.'),
(3,'Icefall','Exposure to icefall.'),
(4,'Cornice','Risk of collapsing cornices.'),
(5,'Serious Avalanche','Highly prone to avalanching.'),
(6,'Serious Rockfall','High incidence of rockfall.'),
(7,'Serious Icefall','High exposure to icefall.'),
(8,'Serious Cornice','High risk of collapsing cornices.'),
(9,'Death Route','Very dangerous and/or frequent accident sites.');
