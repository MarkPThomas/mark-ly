INSERT INTO "climbing_travel_style"
VALUES
(1,'N/A',NULL),
(2,'Cragging','Climbs of Grade I-II where the approach time is usually equal to or less than the climb times.'),
(3,'Single Day','Any climb of grade III or higher, or any outing where the time spent traveling on unbelayed terrain is greater than the time spent on belay.'),
(4,'Multi-day','Any climb that lasts more than a day.'),
(5,'Carryover','Any multiday climb where you cannot return to a camp, and therefore must carry your entire camp up the climb with you. Big walls don\'t count as gear is typicaly hauled.'),
(6,'Expedition','Any multiday climb that takes a week or more, usually involving extreme remoteness and commitment. Usually a large portion of the weight carried is to support the non-climbing activities of approach/deproach and time at camp.'),
(7,'Gym','Climbing at the gym.');
