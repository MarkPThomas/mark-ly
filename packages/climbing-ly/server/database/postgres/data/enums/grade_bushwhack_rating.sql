INSERT INTO "grade_bushwhack_rating"
VALUES
(1,'BW1','Light Brush','Travel mostly unimpeded, only occasional use of hands required (e.g. mature open forest)'),
(2,'BW2','Moderate Brush','Occasional heavy patches. Pace slowed, frequent use of hands required.'),
(3,'BW3','Heavy Brush','Hands needed constantly. Some loss of blood may occur due to scratches and cuts. Travel noticably hindered. Use of four-letter words at times.'),
(4,'BW4-','Severe Brush','Pace less than one mile per hour. Leather gloves and heavy clothing required to avoid loss of blood. Much profanity and mental anguish. Thick stands of brush requiring circumnavigation are encountered.'),
(5,'BW4','Severe Brush','Pace less than one mile per hour. Leather gloves and heavy clothing required to avoid loss of blood. Much profanity and mental anguish. Thick stands of brush requiring circumnavigation are encountered.'),
(6,'BW4+','Severe Brush','Pace less than one mile per hour. Leather gloves and heavy clothing required to avoid loss of blood. Much profanity and mental anguish. Thick stands of brush requiring circumnavigation are encountered.'),
(7,'BW5-','Extreme Brush','Multiple hours needed to travel one mile. Full body armor desirable. Wounds to extremities likely, eye protection needed. Footing difficult due to lack of visibility. Loss of temper inevitable.'),
(8,'BW5','Extreme Brush','Multiple hours needed to travel one mile. Full body armor desirable. Wounds to extremities likely, eye protection needed. Footing difficult due to lack of visibility. Loss of temper inevitable.'),
(9,'BW5+','Extreme Brush','Multiple hours needed to travel one mile. Full body armor desirable. Wounds to extremities likely, eye protection needed. Footing difficult due to lack of visibility. Loss of temper inevitable.');
