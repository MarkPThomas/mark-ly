INSERT INTO "grade_river_crossing_rating"
VALUES
(1,'WA1','A dry crossing is possible by using rocks or logs.'),
(2,'WA2','Possible wet crossing, but a dry crossing can be accomplished with some finesse.'),
(3,'WA3','Wet crossing, ankle- to calf-deep.'),
(4,'WA4','Wet crossing, calf- to knee-deep.'),
(5,'WA5','Wet crossing, greater than knee-deep, possibility of getting swept downstream.');
