INSERT INTO "grade_bushwhack_grade"
VALUES
(1,'I','Brush beating can be done in a few hours or less.'),
(2,'II','Generally will take less than half a day.'),
(3,'III','Could take most of a day, but hardened parties will be able to complete in a short day.'),
(4,'IV','Will take a long day and involve continuous battle.'),
(5,'V','A 1+ to 2-day bushwhack, difficulty rarely less than BW4, large quantities of bandaids and wound dressings will be needed unless properly attired.'),
(6,'VI','The most extreme of bushwhacks, requiring over 2 days to complete with probably a BW5 encountered along the way.');
