INSERT INTO "climbing_type"
VALUES
(1,'Bouldering'),
(2,'Free Climbing'),
(3,'Mountaineering'),
(4,'Aid Climbing'),
(5,'Mixed Climbing'),
(6,'Ice Climbing'),
(7,'Snow Climbing'),
(8,'Glacier'),
(9,'Cross-Country');
