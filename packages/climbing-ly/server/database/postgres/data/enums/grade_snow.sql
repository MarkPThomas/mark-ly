INSERT INTO "grade_snow"
VALUES
(1,'N/A','N/A'),
(2,'< 25 deg','Easy'),
(3,'25-30 deg','Easy'),
(4,'30-35 deg','Moderate'),
(5,'35-40 deg','Moderate'),
(6,'40-45 deg','Moderate'),
(7,'45-50 deg','Steep'),
(8,'50-55 deg','Steep'),
(9,'55-60 deg','Steep'),
(10,'60-65 deg','Very Steep'),
(11,'65-70 deg','Very Steep'),
(12,'70-75 deg','Very Steep'),
(13,'75-80 deg','Very Steep'),
(14,'+80 deg','Vertical');
